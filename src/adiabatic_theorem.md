# Adiabatic approximation

```python inline
import common
from IPython.display import HTML, display
import numpy as np
common.configure_plotting()
```

!!! success "Expected prerequisites"

    Before the start of this lecture, you should be able to:

    - Describe the behaviour of a classical pendulum
    - Describe the behaviour of a quantum particle in a box.

!!! summary "Learning goals"

    After this lecture you will be able to:

    - Explain the adiabatic theorem.
    - Explain how the wavefunction evolves under an adiabatic or abrupt potential.


## Adiabatic theorem

An adiabatic process, in the context of quantum mechanics, is defined by a
gradual change in external conditions with *time*. This is not to be confused with adiabatic
processes in thermodynamics, which is characterised by no energy exchange
between a system and its environment. The **adiabatic theorem**, from Max
Born and Vladimir Fock, states that:


>    A physical system remains in its *instantaneous eigenstate* if a given
>    perturbation is acting on it slowly enough and if there is a gap between the
>    eigenvalue and the rest of the Hamiltonian's spectrum.


Let's unpack what this means through a classical example to solidify our
understanding then apply it to a case involving quantum mechanics.

## Classical Example: Pendulum 
The classical period of a pendulum with arm length $L$ under a gravitational
force $g$ is $T = 2\pi \sqrt{L/g}$. If we now abruptly changed the length of the
pendulum's arm, the motion of the mass at the end would be hard to predict. It
certainly wouldn't resemble the gradual swinging we started with. In contrast,
should we vary $L$ very slowly, we would preserve the oscillation of the mass,
and indeed the period, as a function of time, would now be $T(t) = 2\pi
\sqrt{L(t)/g}$.

Now that we have this intuition, let's look at an example from quantum
mechanics.

## Quantum Example: Particle in a Box
Assume we start with a particle in the ground state of an infinite
one-dimensional box of width $L$. The wavefunction then has the form

$$
\psi_i(x) = \sqrt{\frac{2}{L}} \sin \left( \frac{\pi x}{L} \right).
$$

Now the question we want to answer is: if we change the width of the well by
moving the right wall outwards, how does the wavefunction change? The answer
depends on *how fast* we move one of the walls.

The adiabatic theorem tells us that if system Hamiltonian changes gradually
enough from some initial form $\hat{H}(0)$ to a final form $\hat{H}(T)$, then if
the particle was in the $n^{\text{th}}$ eigenstate of the initial Hamiltonian to
start with, it will be carried to the $n^{\text{th}}$ eigenstate of the final
Hamiltonian. All the while, as the wall is moving from say $L$ to $2L$, the
wavefunction will remain in that ground state associated with whatever width the
well happens to have at any given moment.

We are also now in a position to appreciate the second half of the theorem,
which says \say{if there is a gap between the eigenvalue and the rest of the
  Hamiltonian's spectrum}. In practice, this means that the energy of the
eigenstate we start with has to be non-degenerate and discrete, so that it makes
sense to talk about the $n^{\text{th}}$ eigenstate of one system compared to the
$n^{\text{th}}$ eigenstate of another.

### Abrupt change in potential

First, consider a system where the change in the potential is abrupt. Therefore, the wavefunction goes out of the ground state, and it mixes with high energy states in a non-trivial way. Such behaviour can be observed in the following animation.

```python inline
from adiabatic import make_adiabatic_potential_animation

L = 200
def pot_abrupt(i, t):
    if t < 50:
        width = L/1.7
    else:
        width = L

    return np.tanh((i - width + 30) / 5) + 1 - np.tanh((i - 30) / 5) + 1

anim = make_adiabatic_potential_animation(potential=pot_abrupt)
display(HTML(common.to_svg_jshtml(anim)))
```

### Adiabatic change in potential

On the other hand, for a smooth change in the potential, the adiabatic theorem tells us to expect that the ground state remains in the ground state even as the wall moves, so long as it moves slowly enough.


```python inline

from adiabatic import make_adiabatic_potential_animation

L = 200
def pot_slow(i, t):
    width = L/1.7 + t/600 * (L - L/1.7)

    return np.tanh((i - width + 30) / 5) + 1 - np.tanh((i - 30) / 5) + 1

anim = make_adiabatic_potential_animation(potential=pot_slow)
display(HTML(common.to_svg_jshtml(anim)))

```

## Summary

The adibatic theorem states that a system will remain on its instantaneous eigenstate if:

* The external pertubation acts *slowly* enough.
* There's always a non-zero gap with other states.