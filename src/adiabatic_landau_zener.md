# The Landau-Zener effect

```python inline
import common
from IPython.display import HTML, display
common.configure_plotting()
```
!!! success "Expected prerequisites"

    Before the start of this lecture, you should be able to:

    - Solve a $2\times 2$ Hamiltonian.
    - Explain the adiabatic theorem.

!!! summary "Learning goals"

    After this lecture you will be able to:

    - Describe the role of the velocity in the evolution of a two level system.
    - Describe the interplay between the gap and the velocity during the evolution.
    - Extend the discussion to systems with more energy levels.


## Adiabatic evolution of a quantum system

The problem of two levels crossing is often encountered in quantum mechanics applications. The [Landau](https://www.scirp.org/(S(351jmbntvnsjt1aadkposzje))/reference/ReferencesPapers.aspx?ReferenceID=1566156) and [Zener](https://www.jstor.org/stable/96038?origin=ads) effect describes how a two level system evolves under a time-dependent Hamiltonian. Consider a system described as
$$
H(t) |\Psi(t) \rangle =  E(t) |\Psi(t) \rangle.
$$
The solution of this equation can be writen as,
$$
|\Psi(t) \rangle = \sum_n c_n(t) |\psi_n\rangle.
$$
From the adibatic theorem, we know that the system will remain in the $n-th$ eigenstate if it evolves adiabatically from $t=0$ to $t=T$, that is $|c_0(0)|=|c_0(T)|=1$. However, if the evolution is not sufficiently *slow*, the system will leak to high energy states as $c_{n\neq 0}(T) \neq 0$.

## Two level system

Consider a two level system described by the Hamiltonian,
$$
H(t) = \left( \begin{array}{cc}
vt/2 & \Delta \\
\Delta & -vt/2
\end{array}
\right).
$$
Here $\Delta$ is the coupling between the two levels, $t$ is time, and $v$ is a the velocity at which the Hamiltonian changes. We can find an explicit expression for the energy levels and eigenstates following the usual procedure. The time-dependent state of the system can be written as,
$$
|\Psi(t)\rangle = c_0(t) |0\rangle + c_1(t) |1\rangle.
$$

Landau and Zaner found an explicit expression that describes the probability of tuneling to a high energy state as $t\rightarrow \infty$. It is
$$
P \sim e^{-2\pi \frac{|Delta|^2}{\hbar |v|}}.
$$
Here, $\Delta = \langle 0 | H |1 \rangle$, and $v=d(E_0(t)-E_1(t))/dt$. From the adiabatic theorem, we recall that when the system evolves in an adiabatic way, it will remain in the initial eigenstate as shown in the animation below.

```python inline
import landau_zener as lz

times, ens1, probs1 = lz.landau_zener_data(L=2, v=0.0001, gap=1)
anim = lz.animate_landau_zener(times=times,
                               ens=ens1,
                               probs=probs1,
                               interval=100,
                               L=2,
                               title=r'adiabatic: $v=10^{-3}$')

display(HTML(common.to_svg_jshtml(anim)))
```

However, if the evolution is non-adiabatic, the adiabatic theorem will no longer hold. That is, the system will evolve towards excited states at the gap crossing.

```python inline
import landau_zener as lz

times, ens1, probs1 = lz.landau_zener_data(L=2, v=1, gap=1)
anim = lz.animate_landau_zener(times=times,
                               ens=ens1,
                               probs=probs1,
                               interval=10,
                               L=2,
                               title=r'adiabatic: $v=1$')

display(HTML(common.to_svg_jshtml(anim)))
```

One can observe that in the non-adiabatic case, the system evolves to the excited state. On the other hand, for adiabatic evolution the system remains mostly in the ground state. This evolution can be directly observed in the coefficients of $|\Psi(t)\rangle$ as shown below.

```python inline
import landau_zener as lz
import matplotlib.pyplot as plt

_, _, probs1 = lz.landau_zener_data(v=1, L=2, npts=100, gap=1)
_, _, probs2 = lz.landau_zener_data(v=0.0001, L=2, npts=100, gap=1)

labels=[r'$|c_0(t)|^2$', r'$|c_1(t)|^2$', r'$|c_2(t)|^2$']
colors=['darkblue', 'red', 'green']

fig, ax = plt.subplots(nrows=1, ncols=2, figsize=(10, 4))

i = 0 
for level in probs1.T:
    ax[0].plot(level**2, label=labels[i], c=colors[i])
    i += 1
ax[0].set_ylim(0, 1.1);
i = 0
for level in probs2.T:
    ax[1].plot(level**2, label=labels[i], c=colors[i])
    i += 1

for axes in ax:
    #axes.set_ylabel(r'$|c|_$', fontsize=15)
    axes.set_xlabel(r'$t$', fontsize=15)
    axes.legend(fontsize=12)
    axes.set_xticks([]);

ax[0].set_title(r'non-adiabatic: $v=1$');
ax[1].set_title(r'adiabatic: $v=10^{-3}$');

```

## Gap dependence

The gap between the two states determines the speed required for the process to be adiabatic. Therefore, systems with different gaps will have different behaviours even if they have the same velocity.

```python inline
import landau_zener as lz

times, ens1, probs1 = lz.landau_zener_data(L=2, v=1, gap=1)
_, ens2, probs2 = lz.landau_zener_data(L=2, v=1, gap=0.1)
probs = [probs1, probs2]
ens = [ens1, ens2]
anim = lz.animate_landau_zener_2plots(times=times,
                                      ens=ens,
                                      probs=probs,
                                      interval=50,
                                      title='',
                                      subtitles=[r'$v=1$, $\Delta=1$', r'$v=1$, $\Delta=0.1$']);

display(HTML(common.to_svg_jshtml(anim)))
```

## Systems with more levels

One can expect the intuition of the two-level case to hold for systems with many more levels. That is, if evolution is not adiabatic, interactions of many levels will induce transitions to higher excited states as can be observed in the animation below.

```python inline
import landau_zener as lz

times, ens1, probs1 = lz.landau_zener_data(L=3, v=1, gap=1)
anim = lz.animate_landau_zener(times=times,
                               ens=ens1,
                               probs=probs1,
                               interval=10,
                               L=3,
                               title=r'non-adiabatic: $v=1$');

display(HTML(common.to_svg_jshtml(anim)))
```

## Summary

If the evolution of a system is not adiabatic, the population of the ground state will leak towards high energy states depending on how the levels interact.
