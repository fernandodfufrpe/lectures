site_name: Lecture notes Applications of Quantum Mechanics
repo_url: https://gitlab.kwant-project.org/applications-of-quantum-mechanics/lectures
repo_name: source
edit_uri: edit/master/src/
site_description: |
  Lecture notes Applications of Quantum Mechanics

nav:
  - Intro: 'index.md'
  - WKB approximation:
     - QM in a smooth potential: 'wkb_smooth.md'
     - WKB wave function: 'wkb_wf.md'
     - Connection formulas: 'wkb_connection.md'
     - Bound states: 'wkb_bound.md'
     - Transport: 'wkb_tunnel.md'
  - Adiabatic approximation:
     - Adiabatic theorem: 'adiabatic_theorem.md'
     - Proof of the theorem: 'adiabatic_proof.md'
     - Landau-Zener effect: 'adiabatic_landau_zener.md'
     - Berry phase: 'adiabatic_berry.md'
     - Exercises: 'adiabatic_exercises.md'
theme:
  name: material
  custom_dir: theme
  palette:
    primary: 'white'
    accent: 'indigo'

markdown_extensions:
  - mdx_math:
      enable_dollar_delimiter: True
  - toc:
      permalink: True
  - admonition
  - pymdownx.details
  - pymdownx.extra
  - abbr
  - footnotes
  - meta

extra_css:
  - 'https://use.fontawesome.com/releases/v5.8.1/css/all.css'
  - 'styles/thebelab.css'

extra_javascript:
  - 'https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.3/MathJax.js?config=TeX-AMS_HTML'
  - 'scripts/thebelab-custom.js'
  - 'scripts/mathjaxhelper.js'
  - 'scripts/thebelabhelper.js'
copyright: "Copyright © 2019 Delft University of Technology, CC-BY-SA 4.0."
